<?php

namespace App\Controller;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;
class DocumentController extends FrontendController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function staticPageAction(Request $request): Response
    {
        return $this->render('document/static_page.html.twig');
    }

    /**
     * @Route("/getAllCmsUrls", methods={"GET"}, name="getAllCmsUrls")
     *
    */
    public function getAllCmsUrls(Request $request)
    {
        try{
            
            $params=$request->get('lang');
            $languages = \Pimcore\Tool::getValidLanguages();
            $domain =$_SERVER['HTTP_HOST'];
            $http=$_SERVER['REQUEST_SCHEME'];
            $langResponse=[];
            if($params)
            {
                $languages=explode(',',$params);
            }
            foreach($languages as $lang)
            {
                if($lang)
                {
                    $list = new \Pimcore\Model\DataObject\CmsPage\Listing();
                    $cmsListing=$list->load();
                    $response=[];
                    foreach($cmsListing as $row)
                    {
                        $result['cmsId']=$row->getId();
                        $result['cmsTitle']=$row->getTitle($lang);
                        if($row->getImage($lang))
                        {
                          $result['cmsImage']=$http."://".$domain.$row->getImage($lang)->getPath().$row->getImage($lang)->getFileName();
                        }
                        else
                        {
                            $result['cmsImage']='';
                        }
                        $cmsDocument=$row->getCmsDocument($lang);
                        if($cmsDocument)
                        {
                            $result['cmsUrl']=$http."://".$domain.$cmsDocument->getPath().$cmsDocument->getTitle();
                        }
                        else
                        {
                            $result['cmsUrl']="";
                        }
                        $response[]        =$result;
                    }
                    $langResponse[$lang]=$response;
                }
            }
            if(count($langResponse) > 0)
            {
                $msg="Pages Fetch Successfully";
            }
            else
            {
                $msg="No Pages Found";
            }
            $data['response_code']=200;
            $data['data']=$langResponse;
            $data['messgae']=$msg;
        }
        catch(Exception $e)
        {
            $data['response_code']=500;
            $data['data']=[];
            $data['messgae']="Something's went wrong";
        }
        $jsonResponse = new JsonResponse($data);
        return $jsonResponse;
    }

    
}

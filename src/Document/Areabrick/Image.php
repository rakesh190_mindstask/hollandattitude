<?php

namespace App\Document\Areabrick;

use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;

class Image extends AbstractTemplateAreabrick
{
    public function getName(): string
    {
        return 'Image';
    }

    public function getDescription(): string
    {
        return '';
    }
    
    public function needsReload(): bool
    {
        return false;
    }
}
